{{- define "mautic-exporter.labels" -}}
{{- range $key, $val := $.Values.labels }}
  {{ $key }}: {{ $val -}}
{{ end }}
{{- end -}}